package cn.jbone.cms.common.constant;


public class DictionaryConstant {

    public static final String GROUP_ADS_LOCATION = "ads_location";     //广告位
    public static final String GROUP_ADS_TYPE = "ads_type";             //广告类型
    public static final String GROUP_PLUGIN_TYPE = "plugin_type";       //插件类型



    public static final String ITEM_PLUGIN_TYPE_GLOBAL = "global";      //全局插件
    public static final String ITEM_PLUGIN_TYPE_ARTICLE = "article";      //文章页插件

    public static final String ITEM_ADS_TYPE_IMG = "img";             //广告类型
    public static final String ITEM_ADS_TYPE_TEXT = "text";             //广告类型
    public static final String ITEM_ADS_TYPE_IMG_AND_TEXT = "imgAndText";             //广告类型
    public static final String ITEM_ADS_TYPE_CODE = "code";             //广告类型
}
